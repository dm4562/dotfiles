set shell=/bin/bash
scriptencoding utf-8
set encoding=utf-8
set backspace=2         " backspace in insert mode works like normal editor
syntax on               " syntax highlighting
filetype indent on      " activates indenting for files
set autoindent          " auto indenting
set number              " line numbers
set nobackup            " get rid of anoying ~file
set list
set listchars=tab:>\ ,nbsp:.
set hlsearch

nnoremap <C-H> <C-W><C-H>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>

set foldmethod=syntax
set foldlevel=99

set tabstop=2
set softtabstop=0
set expandtab
set shiftwidth=4
set smarttab
colorscheme desert
